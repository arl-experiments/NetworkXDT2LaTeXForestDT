import networkx as nx
import os
import subprocess
from pathlib import Path


def compile_document(document_path):
    # Source: https://stackoverflow.com/a/34874560
    output_dir_path = str(Path(document_path).parent)
    subprocess.call(["pdflatex", "-output-directory", output_dir_path, document_path])


def generate_latex_document(forest_dt_str, document_path, rotate=False):
    dirname = os.path.dirname(__file__)
    template_path = os.path.join(dirname, "resources/forestdt_template.tex")

    # Source: https://stackoverflow.com/a/10507291
    with open(template_path, "r") as f:
        contents = f.readlines()

    index = 65
    contents.insert(index, forest_dt_str)
    if rotate:
        index = 15
        contents.insert(index, "\\usepackage{rotating}\n")

    with open(document_path, "w") as f:
        contents = "".join(contents)
        f.write(contents)

    if rotate:
        _add_rotating(document_path)


def _add_rotating(document_path):
    # Read in the file
    with open(document_path, 'r') as document_tex:
        filedata = document_tex.read()

    # Replace the target string
    filedata = filedata.replace('figure', 'sidewaysfigure')

    # Write the file out again
    with open(document_path, 'w') as document_tex:
        document_tex.write(filedata)


class DTConverter:
    def __init__(self, T, xhift=2, node_value_styling=True):
        self.T = T
        self.forest_dt_str = ""
        self.xshift = xhift
        self.node_value_styling = node_value_styling

    def _get_child_nodes(self, parent_node):
        bfs_edges = list(nx.bfs_edges(self.T, source=parent_node, depth_limit=1))
        child_nodes = [edge[1] for edge in bfs_edges if edge[1] > parent_node]
        if len(child_nodes) > 2:
            raise Exception("There may only be two child nodes, but there are following: " + str(child_nodes))
        return child_nodes

    def _has_node_parent_node(self, potential_child_node):
        bfs_edges = list(nx.bfs_edges(self.T, source=potential_child_node, depth_limit=1))
        for edge in bfs_edges:
            if edge[1] < potential_child_node:
                return True
        return False

    def _get_root_node(self):
        root_node = None
        for node in self.T.nodes:
            if not self._has_node_parent_node(node):
                if root_node is None:
                    root_node = node
                else:
                    raise Exception("There may not be multiple root nodes", root_node, "and", node)
        return root_node

    def _add_value_weight(self, node, layer, weight):
        weight_str = ""
        elo_str = ""

        if weight is not None:
            weight_str = ";{" + str(weight) + "}"
            xshift = self.xshift
            if weight == 0:
                xshift = -self.xshift
            elo_str = ",elo={xshift=" + str(xshift) + "pt}"
        self.forest_dt_str += layer * 4 * " "
        value_str = str(self.T.nodes[node]["value"])

        if self.node_value_styling:
            value_str = value_str.replace('+', '\\\\+')
            value_str = value_str.replace('.', '&.')

        self.forest_dt_str += "[{$\\begin{aligned}" + value_str + "\end{aligned}$}" + \
                              str(weight_str) + ",plain content" + elo_str + "\n"

        child_nodes = self._get_child_nodes(node)
        if len(child_nodes) > 0:
            print(child_nodes)
            for child_node in child_nodes:
                self._add_value_weight(child_node, layer + 1, self.T.edges[(node, child_node)]["weight"])
        self.forest_dt_str += layer * 4 * " "
        self.forest_dt_str += "] {}\n"

    def convert(self):
        root_node = self._get_root_node()
        root_layer = 0
        self._add_value_weight(root_node, root_layer, None)
        return self.forest_dt_str
