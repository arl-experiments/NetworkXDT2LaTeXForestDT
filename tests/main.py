from src.NetworkXDT2LaTeXForestDT import convert_dt
import networkx as nx
import os

def test_dt_conversion():
    T = nx.balanced_tree(2, 0)

    T.add_node(0, value="0.5x+0.4y>0")

    T.add_node(1, value="0.2x+0.6y>0")
    T.add_node(2, value="0.1x+0.1y")
    T.add_edge(0, 1, weight=0)
    T.add_edge(0, 2, weight=1)

    T.add_node(3, value="0.8x+0.2y")
    T.add_node(4, value="0.9x+0.1y")
    T.add_edge(1, 3, weight=0)
    T.add_edge(1, 4, weight=1)

    print(list(nx.edge_dfs(T)))

    dt_converter = convert_dt.DTConverter(T, node_value_styling=False)
    forest_dt_str = dt_converter.convert()
    print(forest_dt_str)

    dirname = os.path.dirname(__file__)
    output_path = os.path.join(dirname, "../output/forestdt.tex")

    convert_dt.generate_latex_document(forest_dt_str, output_path)
    convert_dt.compile_document(output_path)

if __name__ == '__main__':
    test_dt_conversion()
