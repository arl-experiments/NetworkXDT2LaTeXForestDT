# Overview

This project converts decision trees specified with `networkx` into
`LaTeX` ones with the `forest` package with `decision tree` style.

## Example

As an example (see `test/main.py`) the following can be converted:

```python3
import networkx as nx
T = nx.balanced_tree(2, 0)

T.add_node(0, value="0.5x+0.4y>0")

[...]
```

with

```python3
from src.NetworkXDT2LaTeXForestDT import convert_dt
dt_converter = convert_dt.DTConverter(T)
forest_dt_str = dt_converter.convert()
```

The `forest_dt_str` is inserted into `src/NetworkXDT2LaTeXForestDT/resources/forestdt_template.tex`
and a dummy pdf is generated with:

```python3
convert_dt.generate_latex_document(forest_dt_str, output_path)
convert_dt.compile_document(output_path)
```

It looks like this:

![Compiled decision tree](src/NetworkXDT2LaTeXForestDT/resources/dummy_output_dt.png)
